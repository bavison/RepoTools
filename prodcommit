#!/bin/sh
tmpfile=$(mktemp)
git submodule summary --cached 2>/dev/null | awk '\
/^\*/ {
  path=$2
  split($3, hash, "[.]{3}")
  if (hash[1] == "0000000")
    new_components[path] = hash[2]
  else if (hash[2] == "0000000:")
    removed_components[path] = hash[1]
  else
    changed_components[path] = hash[1] " " hash[2]
}
function get_tag(path, hash) {
  cmd = "cd '\''" path "'\'' && git describe --tags --always " hash
  cmd | getline tag
  close(cmd)
  return tag
}
END {
  # Generate single-line summary giving count of new/removed/changed components
  sep = ""
  if (length(new_components) == 1) {
    printf "1 new component"
    sep = ", "
  } else if (length(new_components) > 1) {
    printf "%d new components", length(new_components)
    sep = ", "
  }
  if (length(removed_components) == 1) {
    printf "%s1 removed component", sep
    sep = ", "
  } else if (length(removed_components) > 1) {
    printf "%s%d removed components", sep, length(removed_components)
    sep = ", "
  }
  if (length(changed_components) == 1)
    printf "%s1 changed component", sep
  else if (length(changed_components) > 1)
    printf "%s%d changed components", sep, length(changed_components)
  printf "\n\n"

  # Detail the new components
  if (length(new_components) > 0) {
    # Header not required if this is the only category
    if (length(removed_components) > 0 || length(changed_components) > 0) {
      if (length(new_components) == 1)
        print "New component"
      else
        print "New components"
    }
    for (path in new_components) {
      printf "  %s (%s)\n", path, get_tag(path, new_components[path])
    }
  }

  # Detail the removed components
  if (length(removed_components) > 0) {
    # Header not required if this is the only category
    if (length(new_components) > 0 || length(changed_components) > 0) {
      if (length(removed_components) == 1)
        print "Removed component"
      else
        print "Removed components"
    }
    # Unfortunately, we cannot look up tags for a removed component, so just report the hash
    for (path in removed_components) {
      printf "  %s (%s)\n", path, removed_components[path]
    }
  }

  # Detail the changed components
  if (length(changed_components) > 0) {
    # Header not required if this is the only category
    if (length(new_components) > 0 || length(removed_components) > 0) {
      if (length(changed_components) == 1)
        print "Changed component"
      else
        print "Changed components"
    }
    for (path in changed_components) {
      split(changed_components[path], hash, " ")
      if (system("cd '\''" path "'\'' && git merge-base --is-ancestor " hash[1] " " hash[2]) == 0)
        suffix = ""
      else if (system("cd '\''" path "'\'' && git merge-base --is-ancestor " hash[2] " " hash[1]) == 0)
        suffix = "  (REGRESSED)"
      else
        suffix = "  (SWITCHED)"
      printf "  %s (%s -> %s)%s\n", path, get_tag(path, hash[1]), get_tag(path, hash[2]), suffix
    }
  }
}' > "$tmpfile"
git commit --edit -F "$tmpfile"
rm "$tmpfile"
